package demo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(name="/scores")
public class ScoreController 
{
	@Autowired
	ScoreRepository repository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Collection<Score> scores()
	{
		ArrayList<Score> scores = new ArrayList<Score>();
		Iterator<Score> i = repository.findAll().iterator();
		while(i.hasNext())
			scores.add(i.next());
		return scores;
	}
	
	/*@RequestMapping(name="/{id}", method=RequestMethod.GET)
	public Score score(@PathVariable int id)
	{
		return repository.findOne(id);
	}*/
	
	@RequestMapping(method=RequestMethod.POST)
	public Score createScore(@RequestParam(value="score", required=true) int score ,@RequestParam(value="username", required=true) String username)
	{
		Score newScore = new Score(score, username);
		repository.save(newScore);
		return newScore;
	}
	
	@RequestMapping(name="/{id}", method=RequestMethod.PUT)
	public Score updateScore(@PathVariable int id, @RequestParam(value="score", required=true) int score ,@RequestParam(value="username", required=true) String username)
	{
		Score updatedScore = repository.findOne(id);
		updatedScore.setScore(score);
		updatedScore.setUsername(username);
		repository.save(updatedScore);
		return updatedScore;
	}
	
	@RequestMapping(name="/{id}", method=RequestMethod.DELETE)
	public void deleteScore(@PathVariable int id)
	{
		repository.delete(id);
	}
	
	
}
