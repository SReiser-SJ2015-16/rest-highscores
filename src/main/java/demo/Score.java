package demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Scores")
public class Score 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int score;
	private String username;
	
	
	protected Score(){}
	
	public Score(int score, String username)
	{
		this.score = score;
		this.username = username;
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int score)
	{
		this.score = score;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}
	
}
